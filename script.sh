#!/bin/bash
# Concat two audio files
# @author: Dariusz Zabrzenski

for entry in "/tmp/audio"/*
do
	for files in $entry/*
	do
		filename=$(basename "${files%.*}")
		extension="${files##*.}"

		# search files
		if [[ $filename == *sideA* ]]
		then
			fileSideA_FLV=$filename"."$extension
			fileSideA_WAV=$filename".wav"
			IFS='_' read -a globalData <<< "$filename"
		else
			fileSideB_FLV=$filename"."$extension
			fileSideB_WAV=$filename".wav"
		fi
	done

	id=basename "$entry"
	sideA=${globalData[2]}
	sideB=${globalData[3]}

	fileOutput=$(echo $id$sideA$sideB|md5sum|awk '{print $1}')
	output="$entry/$fileOutput.wav"

	wav_1="ffmpeg -i $entry/$fileSideA_FLV $entry/$fileSideA_WAV"
	wav_2="ffmpeg -i $entry/$fileSideB_FLV $entry/$fileSideB_WAV"

	eval $wav_1
	eval $wav_2

	duration_1="$(soxi -D $entry/$fileSideA_WAV | awk '{printf("%d\n",$1 + 0.5)}')"
	duration_2="$(soxi -D $entry/$fileSideB_WAV | awk '{printf("%d\n",$1 + 0.5)}')"

	if [ "$duration_1" -gt "$duration_2" ]
	then
		diff="$(expr $duration_1 - $duration_2)"
		eval "sox $entry/$fileSideB_WAV $entry/temp_1.wav pad $diff"
		eval "sox -m $entry/$fileSideA_WAV $entry/temp_1.wav $output"

		# remove temporary file
		eval "rm $entry/temp_1.wav"
	else
		diff="$(expr $duration_2 - $duration_1)"
		eval "sox $entry/$fileSideA_WAV $entry/temp_2.wav pad $diff"
		eval "sox -m $entry/$fileSideB_WAV $entry/temp_2.wav $output"

		# remove temporary file
		eval "rm $entry/temp_2.wav"
	fi

	# remove A and B file
	eval "rm $entry/$fileSideA_WAV"
	eval "rm $entry/$fileSideB_WAV"
done
